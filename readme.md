# Системные требования
Для работы проекта требуются:  

1. PHP7.0+ (Расширения: php-pdo, mcryptp, openssl, mbstring, tokenizer, xml, php-json)
2. MySQL 5.6+ или MariaDB
3. Composer (файл composer.phar)
4. NodeJS 10+ вместе с npm (лучше всего поставить через NVM)

# Развертывание проекта
1. Клонировать проект с помощью команды `git clone git@bitbucket.org:Trackerzod/golovachev_av_goodline_test.git`
2. В папке проекта выполнить команду `composer install`, если Composer не установлен глобально то можно воспользоваться файлом **_composer.phar_** в проекте и выполнить команду `php composer.phar install`
3. Скопировать файл **_.env.example_** с именем **_.env_**
4. Сгенерировать ключ приложения комендой `php artisan key:generate`
5. В файле **_.env_** указать имя базы данных, логин и пароль пользователя БД
6. Создать базу данных и пользователя в MySQL с именами из файла **_.env_**
7. Выполнить миграцию БД командой `php artican migrate`
8. Выполнить заполение тестовыми данными командой 'php artisan db:seed`
9. Запустить приложение командой `php artisan serv`
10. Для оперативной компиляции стилей и скриптов нужно открыть папку во второй консоли и выполнить команду `npm run dev`

# Модели
## Структура модели пасты (Paste)  
Поля: name, content, expiration(время жизни), expouse(публичная - частная), syntax_format_id, slug(уникальный хэш по которому осуществляется доступ)  
Связана с моделью SyntaxFormat

## Прочие модели
Модель списка ЯП (SyntaxFormat)  
Поля: name
Связана с моделью Paste