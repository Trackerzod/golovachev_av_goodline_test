<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Paste;
use App\SyntaxFormat;

class IndexController extends Controller
{
    public function index() {
        $pastes = $this->getPastes();
        $exposes = Paste::EXPOUSE;
        $time_intervals = Paste::EXPIRATION;
        $syntax_formates = SyntaxFormat::all();
        return view('index.index', [
            'pastes' => $pastes,
            'exposes' => $exposes,
            'time_intervals' => $time_intervals,
            'syntax_formates' => $syntax_formates
        ]);
    }
}
