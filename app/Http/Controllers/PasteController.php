<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Paste;
use App\SyntaxFormat;
use App\Http\Requests\StorePaste;
use \DateTime;
use \DateInterval;

class PasteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StorePaste  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePaste $request)
    {
        $paste = new Paste();
        $syntax_format = SyntaxFormat::find($request->syntax_format);

        $paste->name = is_null($request->name) || empty($request->name) ? 
            'Untitled' : $request->name;
        $paste->content = $request->content;
        $paste->slug = Paste::generateHash();
        $paste->expouse = $request->expouse;
        $paste->syntaxFormat()->associate($syntax_format);
        $date_expiration = new DateTime();
        switch($request->expiration) {
            case "N":
                $paste->expiration = null;
            break;

            case "10M" || "1H" || "3H":
                $paste->expiration = $date_expiration->add(new DateInterval("PT".$request->expiration));
            break;

            default:
                $paste->expiration = $date_expiration->add(new DateInterval("P".$request->expiration));
            break;
        }
        $paste->save();
        
        return redirect($paste->slug);        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $paste = Paste::where('slug', $id)->first();
        $now = new DateTime();
        $now = $now->format('Y-m-d H:i:s');

        if (is_null($paste->expiration) || strtotime($now) < strtotime($paste->expiration)) {
            $pastes = $this->getPastes();
            return view('paste.show', [
                'paste' => $paste,
                'pastes' => $pastes,
            ]);
        } else {
            return redirect('/')->with('status', 'Paste expiration is outdated!');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
