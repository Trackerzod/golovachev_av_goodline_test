<?php

/*
name    string(255)
*/

namespace App;
use App\Paste;

use Illuminate\Database\Eloquent\Model;

class SyntaxFormat extends Model
{
    public function pastes() {
        return $this->hasMany(Paste::class);
    }
}
