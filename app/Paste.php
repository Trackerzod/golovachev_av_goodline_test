<?php
/*
Table 'pastes'

name                  string(255)
content               text
expiration            datetime
expouse               string(255)
slug                  string(255)
syntax_format_id      integer

pastes_syntax_format_id_foreign  syntax_format_id
*/

namespace App;

use Illuminate\Database\Eloquent\Model;
use \DateTime;
use App\SyntaxFormat;

class Paste extends Model
{

    public const EXPOUSE = [
        'public',
        'unlisted'
    ];

    public const EXPIRATION = [
        'N' => 'Never',
        '10M' => '10 Minutes',
        '1H' => '1 Hour', 
        '3H' => '3 Hours',
        '1D' => '1 Day',
        '7D' => '1 Week',
        '1M' => '1 Month'
    ];

    public function syntaxFormat() {
        return $this->belongsTo(SyntaxFormat::class);
    }

    /**
     * Generate unique random hash for paste's slug.
     *
     * @return string
     */
    public static function generateHash() {
        $hash = '';

        do {
            $hash = md5(uniqid(rand(), true));
            $existed_paste = self::where('slug', $hash)->get();
        } while(!$existed_paste->isEmpty());

        return $hash;
    }

    /**
     * Scope for public non outdated pastes.
     *
     * @return Illuminate\Database\Eloquent\Collection
     */
    public static function getPublicPastes() {
        return Paste::where('expouse', self::EXPOUSE[0])
                ->where(function($q) {
                    $q->where('expiration', '>', new DateTime())
                    ->orWhere('expiration', null);
                })
                ->orderBy('created_at', 'desc')
                ->limit(10)
                ->get();
    }     
}
