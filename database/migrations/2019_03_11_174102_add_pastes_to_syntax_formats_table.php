<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPastesToSyntaxFormatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pastes', function (Blueprint $table) {
            $table->integer('syntax_format_id')->unsigned()->nullable();
            $table->foreign('syntax_format_id')->references('id')->on('syntax_formats');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pastes', function (Blueprint $table) {
            $table->dropForeign('pastes_syntax_format_id_foreign');
            $table->dropColumn('syntax_format_id');
        });
    }
}
