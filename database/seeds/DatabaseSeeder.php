<?php

use Illuminate\Database\Seeder;
use App\Paste;
use App\SyntaxFormat;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Filling pastes
        DB::table('pastes')->delete();
        
        for($i = 0; $i < 10; $i++) {
            $paste = new Paste();
            $paste->name = "Paste$i";
            $paste->content = "Content$i";
            $paste->slug = Paste::generateHash();
            $paste->expouse = Paste::EXPOUSE[rand(0,1)];
            $paste->save();
        }

        //Filling syntax format
        $syntaxes = [
            "JavaScript",
            "PHP",
            "Java",
            "C++",
            "C#",
            "CSS",
            "HTML",
            "Ruby"
        ];

        DB::table('syntax_formats')->delete();
        foreach($syntaxes as $syntax) {
            $syntax_format = new SyntaxFormat();
            $syntax_format->name = $syntax;
            $syntax_format->save();
        }
    }
}
