<div class="row">
    @include('common.errors')
    <form class="col s12" action="{{ url('paste') }}" method="POST">
        {{ csrf_field() }}
        <div class="row">
            <div class="input-field col s12">
                <textarea id="paste-content" name="content" class="materialize-textarea"></textarea>
                <label for="content">New paste</label>
            </div>
        </div>
        <div class="row">
            <div class="input-field col s12">
                <select id="paste-syntax-format" name="syntax_format">
                <option value="" disabled selected>None</option>
                    @foreach ($syntax_formates as $syntax)
                        <option value="{{ $syntax->id }}">{{ $syntax->name }}</option>                
                    @endforeach
                </select>
                <label>Syntax Highlighting</label>
            </div>
        </div>
        <div class="row">
            <div class="input-field col s12">
                <select id="paste-expiration" name="expiration">
                    @foreach ($time_intervals as $key => $interval)
                        <option value="{{ $key }}">{{ $interval }}</option>                
                    @endforeach
                </select>
                <label>Paste Expiration</label>
            </div>
        </div>
        <div class="row">
            <div class="input-field col s12">
                <select id="paste-expouse" name="expouse">
                    @foreach ($exposes as $option)
                        <option value="{{ $option }}">{{ ucfirst($option) }}</div>
                    @endforeach
                </select>
                <label>Paste Exposure</label>
            </div>
        </div>
        <div class="row">
            <div class="input-field col s6">
                <input id="paste-name" type="text" name="name" class="validate">
                <label for="name">Paste Name / Title</label>
            </div>
        </div>
        <div class="row">
            <div class="input-field col s6">
                <button class="waves-effect waves-light btn-large" type="submit" name="submit">Create</button>     
            </div>
        </div>
    </form>
</div>