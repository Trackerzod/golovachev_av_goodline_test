<!DOCTYPE html>
<html lang="ru">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ config('app.name') }}</title>        

        <link rel="stylesheet" href="{{ asset('/css/app.css') }}"/>
        <script src="{{ asset('/js/app.js') }}"></script>
    </head>
    <body>
        <header>
            <nav>
                <div class="container">
                    <div class="nav-wrapper">
                        <a href="{{ url('/') }}" class="brand-logo">{{ config('app.name') }}</a>            
                    </div>
                </div>
            </nav>
        </header>
        <div class="container">
            <div class="row">
                <div class="col s12">
                    @if (session('status'))
                        <div class="alert alert-success">
                            <span class="red-text darken-3">
                                {{ session('status') }}
                            </span>
                        </div>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col s9">
                    @yield('content')
                </div>
                <div class="col s3">
                @include('common.sidebar')
                </div>
            </div>
        </div>
    </body>
</html>
