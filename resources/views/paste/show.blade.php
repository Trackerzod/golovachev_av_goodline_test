@extends('layout.app')
@section('content')

<div class="row">
    <div class="col s12">
        <h4>{{ $paste->name }}</h2>
    </div>
</div>
<div class="row">
    <div class="col s8">
        <div id="editor-content" data-syntax="{{ is_null($paste->syntaxFormat) ? '' : $paste->syntaxFormat->name }}">
        </div>
        <br/>
        <div class="row">
            <div class="input-field col s12">
                <textarea id="user-paste" class="materialize-textarea">{{ $paste->content }}</textarea>
                <label for="content">Raw text</label>
            </div>
        </div>
    </div>
</div>

@endsection