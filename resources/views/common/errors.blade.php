@if (count($errors) > 0)
    <div class="col s12">
        @foreach($errors->all() as $error)
        <div class="row">
            <div class="col s12">
                {{ $error }}
            </div>
        </div>
        @endforeach
    </div>
@endif