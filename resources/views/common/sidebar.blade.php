<ul class="collection with-header">
    <li class="collection-header"><h4>Public Pastes</h4></li>
    @foreach ($pastes as $paste)
        <a href="{{ route('paste.show', ['slug' => $paste->slug]) }}" class="collection-item">{{ $paste->name }}</li>
    @endforeach                
</ul>